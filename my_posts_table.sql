/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : egg04

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 11/01/2019 20:56:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for my_posts_table
-- ----------------------------
DROP TABLE IF EXISTS `my_posts_table`;
CREATE TABLE `my_posts_table`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `post_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime(0) DEFAULT NULL,
  `modified_at` datetime(0) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of my_posts_table
-- ----------------------------
INSERT INTO `my_posts_table` VALUES (1, '18917001878', 'qwqwdqwdwddd', 1, '2019-01-11 20:52:56', '2019-01-11 20:52:56');

SET FOREIGN_KEY_CHECKS = 1;
