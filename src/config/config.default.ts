export = (appInfo: any) => {
  const config: any = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1547208163178_346';

  // add your config here
  config.middleware = [
  ];

  // watch default file state
  config.development = {
    watchDirs: [
      'app',
      'lib',
      'config',
      'app.ts',
      'agent.ts',
      'interface.ts',
    ],
    overrideDefault: true,
  };

  //mysql配置开始
  config.sequelize = {
    dialect: 'mysql', // support: mysql, mariadb, postgres, mssql
    dialectOptions: {
      charset: 'utf8mb4',
    },
    database: 'egg04',
    host: 'localhost',
    port: '3306',
    username: 'root',
    password: 'root',
    timezone: '+08:00',
  };
  //mysql配置结束

  //cors配置开始
  config.security = {
    csrf: false,
    domainWhiteList: ['http://localhost:7001'],
  };
  config.cors = {
    credentials: true,
  };
  //cors配置结束

  return config;
};
